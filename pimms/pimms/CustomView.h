//
//  CustomView.h
//  oniradar
//
//  Created by kunihiro takedomi on 2015/05/19.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
- (void)fadeOutWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay;

@end
