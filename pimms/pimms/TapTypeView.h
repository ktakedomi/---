//
//  TapTypeView.h
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/11.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TapTypeView : UIView

@property(nonatomic)    NSInteger   tapType;
@property(nonatomic)    NSInteger   step;
@property(nonatomic)    UIImageView *ivMessage;

- (id)initWithFrame:(CGRect)frame type:(NSInteger)type;
- (void)startAnimate;

@end
