//
//  main.m
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/04.
//  Copyright © 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
