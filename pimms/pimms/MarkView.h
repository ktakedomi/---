//
//  MarkView.h
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/04.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#define MARK_WIDTH_S      250.0f
#define MARK_HEIGHT_S     250.0f
#define MARK_WIDTH_E      100.0f
#define MARK_HEIGHT_E     100.0f
#define MARK_ANIME_RATE   60.0f

#define MARK_TOUCH_PERFECT  0.05f
#define MARK_TOUCH_GRATE    0.1f
#define MARK_TOUCH_GOOD     0.2f


enum{
    TapTypeMiss,
    TapTypeGood,
    TapTypeGrate,
    TapTypePerfect
}typedef TapType;

typedef void (^tapFunction)(UIView*);

@interface MarkView : UIView{
    tapFunction    _tapFnc;
}

@property(nonatomic) CGFloat        duration;
@property(nonatomic) CGFloat        sx;
@property(nonatomic) CGFloat        sy;
@property(nonatomic) CGFloat        ex;
@property(nonatomic) CGFloat        ey;
@property(nonatomic) CGFloat        colorR;
@property(nonatomic) CGFloat        colorG;
@property(nonatomic) CGFloat        colorB;
@property(nonatomic) CGFloat        rate;
@property(nonatomic) NSInteger      step;
@property(nonatomic) NSTimeInterval currentTime;
@property(nonatomic) NSTimeInterval touchTime;
@property(nonatomic) NSTimer        *tmAnime;
@property(nonatomic) BOOL           touch;
@property(nonatomic) AVAudioPlayer  *player;
@property(nonatomic) UIImageView    *ivStar;
@property(nonatomic) UIImage        *imParticle;

- (void)initTargetWithDuration:(CGFloat)duration x:(CGFloat)x y:(CGFloat)y r:(CGFloat)r g:(CGFloat)g b:(CGFloat)b tapFunction:(tapFunction)fnc;
- (void)animeLoop;
- (void)fadeAnime;
- (NSInteger)tapType;

@end
