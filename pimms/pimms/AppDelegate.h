//
//  AppDelegate.h
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/04.
//  Copyright © 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic,strong)    IBOutlet UIWindow               *window;
@property (nonatomic,strong)    IBOutlet UINavigationController *navi;


@end

