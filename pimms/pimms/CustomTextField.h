//
//  CustomTextField.h
//  oniradar
//
//  Created by kunihiro takedomi on 2015/05/16.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat height;
@property (nonatomic) IBInspectable UIColor *placeholderColor;

@end
