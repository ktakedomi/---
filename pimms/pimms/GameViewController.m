//
//  GameViewController.m
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/04.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import "GameViewController.h"
#import "MarkView.h"
#import "TapTypeView.h"

@interface GameViewController ()

@end


@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _vwCurtain.alpha = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//-----------------------------------------------------------------------------------------
#pragma mark UI
//-----------------------------------------------------------------------------------------
- (IBAction)pusuStartButton:(id)sender{
    //ボタン非表示
    _btStart.hidden = YES;
    
    //幕をフェードアウト
    [UIView animateWithDuration:3.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _vwCurtain.alpha = 0;
                     }completion:^(BOOL finished) {
                         //BGM再生開始
                         NSError     *error  = nil;
                         NSString    *path   = [[NSBundle mainBundle] pathForResource:@"bgm_01" ofType:@"caf"];
                         NSURL       *url    = [[NSURL alloc] initFileURLWithPath:path];
                         _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                         _player.volume = 0.1;
                         
                         [_player setDelegate:self];
                         [_player prepareToPlay];
                         [_player play];
                         
                         path   = [[NSBundle mainBundle] pathForResource:@"tap" ofType:@"caf"];
                         url    = [[NSURL alloc] initFileURLWithPath:path];
                         _player2 = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                         
                         [_player2 setDelegate:self];
                         [_player2 prepareToPlay];
                         
                         _beetTime       = 60/125.05f;
                         _mainLoopTime   = 0;
                         _loopStartTime  = [[NSDate date] timeIntervalSince1970];
                         _tmMainLoop     = [NSTimer scheduledTimerWithTimeInterval:1/60.0f target:self selector:@selector(mainLoop) userInfo:nil repeats:YES];
                         
                         _vwCurtain.userInteractionEnabled  = NO;
                         _btStart.userInteractionEnabled    = NO;
                     }];
    
    
}
- (void)tapMarkWithMark:(UIView*)sender{
    MarkView    *mv = (MarkView*)sender;
    TapTypeView *tv = [[TapTypeView alloc] initWithFrame:_ivTapType.frame type:[mv tapType]];
    
    [_vwMark addSubview:tv];
    [tv startAnimate];
    
    NSLog(@"tap event");
}
//-----------------------------------------------------------------------------------------
#pragma mark MainLoop
//-----------------------------------------------------------------------------------------
- (void)mainLoop{
    if( _player.currentTime < _mainLoopTime)  return;

    _mainLoopTime += _beetTime;
    
    NSInteger   cnt = 0;
    if( rand()%4 < 2 ){
        for(int i=0; i<[_vwStage.subviews count]; i++){
            if( i == rand()%[_vwStage.subviews count] )
            {
                //[_player2 play];
                UIView      *cv = [_vwStage.subviews objectAtIndex:i];
                MarkView    *mv = [[MarkView alloc] init];
                
                if( i == 0 ){
                    [mv initTargetWithDuration:_beetTime x:cv.center.x y:cv.center.y r:1 g:0 b:0
                                   tapFunction:^(UIView *vw){[self tapMarkWithMark:vw];}
                     ];
                }else if( i == 1 ){
                    [mv initTargetWithDuration:_beetTime x:cv.center.x y:cv.center.y r:1 g:1 b:0 tapFunction:^(UIView *vw){[self tapMarkWithMark:vw];}];
                }else if( i == 2 ){
                    [mv initTargetWithDuration:_beetTime x:cv.center.x y:cv.center.y r:1 g:0.75 b:0 tapFunction:^(UIView *vw){[self tapMarkWithMark:vw];}];
                }
                [_vwMark addSubview:mv];
                [_vwMark sendSubviewToBack:mv];
                cnt++;
                if( cnt >= 2 )  break;
            }
        }
    }
}

@end
