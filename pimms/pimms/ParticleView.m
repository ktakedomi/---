//
//  ParticleView.m
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/09.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import "ParticleView.h"

@implementation ParticleView

- (void)fadeOutWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay{
    [UIView animateWithDuration:duration
                          delay:delay
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.frame = CGRectMake(self.center.x, self.center.y, 1, 1);
                         self.alpha = 0;
                     }completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
