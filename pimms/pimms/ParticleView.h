//
//  ParticleView.h
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/09.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParticleView : UIImageView

- (void)fadeOutWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay;

@end
