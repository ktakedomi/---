//
//  GameViewController.h
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/04.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface GameViewController : UIViewController<AVAudioPlayerDelegate>

@property(nonatomic) AVAudioPlayer  *player;
@property(nonatomic) AVAudioPlayer  *player2;
@property(nonatomic) NSTimer        *tmMainLoop;
@property(nonatomic) NSTimeInterval beetTime;
@property(nonatomic) NSTimeInterval mainLoopTime;
@property(nonatomic) NSTimeInterval loopStartTime;

@property IBOutlet  UIView      *vwStage;
@property IBOutlet  UIView      *vwMark;
@property IBOutlet  UIView      *vwCurtain;
@property IBOutlet  UIButton    *btStart;
@property IBOutlet  UIImageView *ivTapType;

- (void)tapMarkWithMark:(UIView*)sender;

@end
