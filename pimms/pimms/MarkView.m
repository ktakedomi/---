//
//  MarkView.m
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/04.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import "MarkView.h"
#import "ParticleView.h"

@implementation MarkView

//-----------------------------------------------------------------------------------------
/*
 初期化
 */
- (void)initTargetWithDuration:(CGFloat)duration x:(CGFloat)x y:(CGFloat)y r:(CGFloat)r g:(CGFloat)g b:(CGFloat)b tapFunction:(tapFunction)fnc{
    CGFloat rg = (2*M_PI)*(rand()%180)/360;
    _duration   = duration;
    _sx         = x + sin(rg - M_PI_2)*100;
    _sy         = y - cos(rg - M_PI_2)*100 - 50;
    _ex         = x;
    _ey         = y;
    _step       = 0;
    _colorR     = r;
    _colorG     = g;
    _colorB     = b;
    _currentTime = 0;
    _touchTime  = [[NSDate date] timeIntervalSince1970];
    _touch      = NO;
    _tapFnc     = fnc;
    
    _ivStar     = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mark-01.png"]];
    _ivStar.contentMode = UIViewContentModeScaleAspectFit;
    _ivStar.hidden      = YES;
    [self addSubview:_ivStar];
    
    self.backgroundColor        = [UIColor clearColor];
    self.userInteractionEnabled = YES;
    
    NSError     *error  = nil;
    NSString    *path   = [[NSBundle mainBundle] pathForResource:@"tap" ofType:@"caf"];
    NSURL       *url    = [[NSURL alloc] initFileURLWithPath:path];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [_player prepareToPlay];
     /**/
    
    _tmAnime = [NSTimer scheduledTimerWithTimeInterval:1.0f/MARK_ANIME_RATE target:self selector:@selector(animeLoop) userInfo:nil repeats:YES];
    
    _imParticle = [[UIImage imageNamed:@"star.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    //self.layer.shadowOffset = CGSizeMake(0, 2); // 上向きの影
    //self.layer.shadowOpacity = 0.8;
}
//-----------------------------------------------------------------------------------------
#pragma mark Animation
//-----------------------------------------------------------------------------------------
/*
 アニメ実行
 */
- (void)animeLoop{
    switch (_step) {
        case 0:{
            _rate = _currentTime/(_duration*2);
            CGRect          src  = CGRectMake(_sx - (MARK_WIDTH_S/2), _sy - (MARK_HEIGHT_S/2), MARK_WIDTH_S, MARK_HEIGHT_S);
            CGRect          dest = CGRectMake(_ex - (MARK_WIDTH_E/2), _ey - (MARK_HEIGHT_E/2), MARK_WIDTH_E, MARK_HEIGHT_E);
            
            self.frame  = CGRectMake(   (dest.origin.x - src.origin.x)*_rate + src.origin.x,
                                     (dest.origin.y - src.origin.y)*_rate + src.origin.y,
                                     (dest.size.width - src.size.width)*_rate + src.size.width,
                                     (dest.size.height - src.size.height)*_rate + src.size.height);
            
            self.alpha = 0.3*_rate + 0.7;
            
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= (_duration*2) ){
                _step++;
                _currentTime -= (_duration*2);
            }
        }
            break;
        case 1:{
            _colorR = 1;
            _colorG = 1;
            _colorB = 1;
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= (_duration/4) ){
                _step++;
                _currentTime -= (_duration/4);
            }
            break;
        }
        case 2:{
            _rate = _currentTime/(_duration/4);
            self.alpha = 1 - _rate;
            
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= (_duration/4) ){
                [_tmAnime invalidate];
                [self removeFromSuperview];
            }
        }
            break;
    }
    
    
    
    /*
    switch (_step) {
        //出現アニメ
        case 0:
        {
            if( _currentTime >= _duration/4 ){
                _currentTime = _duration/4;
            }
            _rate = _currentTime/(_duration/4);
            
            CGRect          src  = CGRectMake(_sx - (MARK_WIDTH_S/2), _sy - (MARK_HEIGHT_S/2), MARK_WIDTH_S, MARK_HEIGHT_S);
            CGRect          dest = CGRectMake(_sx - (MARK_WIDTH_S*1.2/2), _sy - (MARK_HEIGHT_S*1.2/2), MARK_WIDTH_S*1.2, MARK_HEIGHT_S*1.2);
            
            self.frame  = CGRectMake(   (dest.origin.x - src.origin.x)*_rate + src.origin.x,
                                        (dest.origin.y - src.origin.y)*_rate + src.origin.y,
                                        (dest.size.width - src.size.width)*_rate + src.size.width,
                                        (dest.size.height - src.size.height)*_rate + src.size.height
                                     );
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= _duration/4 ){
                _step++;
                _currentTime = 0;
            }
            break;
        }
        case 1:
        {
            if( _currentTime >= _duration/4 ){
                _currentTime = _duration/4;
            }
            _rate = _currentTime/(_duration/4);
            
            CGRect          src  = CGRectMake(_sx - (MARK_WIDTH_S*1.2/2), _sy - (MARK_HEIGHT_S*1.2/2), MARK_WIDTH_S*1.2, MARK_HEIGHT_S*1.2);
            CGRect          dest = CGRectMake(_sx - (MARK_WIDTH_S/2), _sy - (MARK_HEIGHT_S/2), MARK_WIDTH_S, MARK_HEIGHT_S);
            self.frame  = CGRectMake(   (dest.origin.x - src.origin.x)*_rate + src.origin.x,
                                     (dest.origin.y - src.origin.y)*_rate + src.origin.y,
                                     (dest.size.width - src.size.width)*_rate + src.size.width,
                                     (dest.size.height - src.size.height)*_rate + src.size.height
                                     );
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= _duration/4 ){
                _step++;
                _currentTime = 0;
            }
            break;
        }
        // 回転 & スケール
        case 2:
        case 4:
        case 6:
        {
            if( _currentTime < _duration/2 ){
                _currentTime += 1.0f/MARK_ANIME_RATE;
                break;
            }
            _rate = (_currentTime - _duration/2) / (_duration/4);
            if(_rate > 1) _rate = 1;
            
            NSInteger  s   = _step/2;
            CGPoint    ps;
            if( _step == 2 ) ps  = CGPointMake(_sx, _sy);
            else             ps  = CGPointMake((_ex - _sx)*(s-1)/3 + _sx, (_ey - _sy)*(s-1)/3 + _sy);
            CGPoint    pd  = CGPointMake((_ex - _sx)*s/3 + _sx, (_ey - _sy)*s/3 + _sy);
            CGFloat    w   = (MARK_WIDTH_E - MARK_WIDTH_S)*s/3 + MARK_WIDTH_S;
            CGFloat    h   = (MARK_HEIGHT_E - MARK_HEIGHT_S)*s/3 + MARK_HEIGHT_S;
            CGRect     src  = CGRectMake(ps.x - w/2,        ps.y - h/2, w, h);
            CGRect     dest = CGRectMake(pd.x - w*1.1/2,    pd.y - h*1.1/2, w*1.1, h*1.1);
            
            self.frame  = CGRectMake(   (dest.origin.x - src.origin.x)*_rate + src.origin.x,
                                     (dest.origin.y - src.origin.y)*_rate + src.origin.y,
                                     (dest.size.width - src.size.width)*_rate + src.size.width,
                                     (dest.size.height - src.size.height)*_rate + src.size.height
                                     );
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= _duration*3/4 ){
                _step++;
                _rate = 0;
                _currentTime = 0;
            }
            break;
        }
        case 3:
        case 5:
        case 7:
        {
            _rate = _currentTime / (_duration/4);
            if(_rate > 1) _rate = 1;
            
            NSInteger  s   = (int)_step/2;
            CGPoint    pos = CGPointMake((_ex - _sx)*s/3 + _sx, (_ey - _sy)*s/3 + _sy);
            CGFloat    w   = (MARK_WIDTH_E - MARK_WIDTH_S)*s/3 + MARK_WIDTH_S;
            CGFloat    h   = (MARK_HEIGHT_E - MARK_HEIGHT_S)*s/3 + MARK_HEIGHT_S;
            CGRect     src  = CGRectMake(pos.x - w*1.1/2, pos.y - h*1.1/2, w*1.1, h*1.1);
            CGRect     dest = CGRectMake(pos.x - w/2, pos.y - h/2, w, h);
            
            self.frame  = CGRectMake(   (dest.origin.x - src.origin.x)*_rate + src.origin.x,
                                     (dest.origin.y - src.origin.y)*_rate + src.origin.y,
                                     (dest.size.width - src.size.width)*_rate + src.size.width,
                                     (dest.size.height - src.size.height)*_rate + src.size.height
                                     );
            
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= _duration/4 ){
                _step++;
                _rate = 0;
                _currentTime = 0;
            }
            break;
        }
        case 8:
        {
            if( _currentTime < _duration/2 ){
                _currentTime += 1.0f/MARK_ANIME_RATE;
                break;
            }
            _rate = (_currentTime - _duration/2) / (_duration/2);
            if(_rate > 1) _rate = 1;
            self.alpha = 1 - _rate;
            
            _currentTime += 1.0f/MARK_ANIME_RATE;
            if( _currentTime >= _duration ){
                [_tmAnime invalidate];
                [self removeFromSuperview];
            }
        }
    }/**/
    [self setNeedsDisplay];
    
    //見逃したらMISS
    NSTimeInterval   sub = [[NSDate date] timeIntervalSince1970] - _touchTime - _duration*2;
    if( self.userInteractionEnabled && sub >= MARK_TOUCH_GOOD ){
        _touch = YES;
        //[_player play];
        self.userInteractionEnabled = NO;
        
        if( _tapFnc )   _tapFnc(self);
    }
}

//-----------------------------------------------------------------------------------------
/*
 マークの描画
 */
- (void)drawRect:(CGRect)rect {
    static int counter = 0;
    
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    UIBezierPath    *path;
    CGPoint         posStar = CGPointMake(0, -self.frame.size.height/2 + 6);
    
    //タイマーライン
    {
        CGFloat r = (_colorR + 1)*0.9;
        CGFloat g = (_colorG + 1)*0.9;
        CGFloat b = (_colorB + 1)*0.9;
        CGFloat rate = _currentTime / (_duration*2);
        CGFloat rg = 0;
        
        if(r > 1) r = 1;
        if(g > 1) g = 1;
        if(b > 1) b = 1;
        
        if( _touch )    CGContextSetRGBStrokeColor(context, 1, 1, 1, 1);
        else            CGContextSetRGBStrokeColor(context, r, g, b, 1);
        path = [UIBezierPath bezierPath];
        path.lineWidth = 5;
        
        if( _step == 0 ){
            rg = (M_PI*2*rate) - M_PI_2;
        }else{
            rg = (M_PI*2) - M_PI_2;
        }

        [path addArcWithCenter:center radius:(self.frame.size.width/2 - 6) startAngle:-M_PI_2 endAngle:rg clockwise:YES];
        [path stroke];
        
        CGPoint old = posStar;
        rg += M_PI_2;
        posStar.x = cos(rg)*old.x - sin(rg)*old.y;
        posStar.y = sin(rg)*old.x + cos(rg)*old.y;
        
        //パーティクル登録
        /*
        if( _step == 0 && counter%2 == 0 )
        {
            CGFloat r = (_colorR + 1)*0.5;
            CGFloat g = (_colorG + 1)*0.5;
            CGFloat b = (_colorB + 1)*0.5;
            
            if(r > 1) r = 1;
            if(g > 1) g = 1;
            if(b > 1) b = 1;
            
            for(int i=0; i<2; i++)
            {
                CGFloat size = rand()%20+7;
                CGPoint pos  = CGPointMake(rand()%20-10, rand()%20-10);
                ParticleView *iv = [[ParticleView alloc] initWithImage:_imParticle];
                iv.frame = CGRectMake(self.frame.size.width/2 + posStar.x + pos.x - size/2, self.frame.size.height/2 + posStar.y + pos.y - size/2, size, size);
                iv.contentMode    = UIViewContentModeScaleAspectFit;
                iv.tintColor      = [UIColor colorWithRed:r green:g blue:b alpha:1];
                iv.tag            = 10;
                [self addSubview:iv];
            }
        }/**/
    }
    /*
    if( _step >= 2 )
    {
        NSInteger   cnt = (int)(_step/2);
        
        CGFloat r = (_colorR + 1)*0.9;
        CGFloat g = (_colorG + 1)*0.9;
        CGFloat b = (_colorB + 1)*0.9;
        CGFloat rate = _currentTime / (_duration/4);
        CGFloat rg = 0;
        
        if(r > 1) r = 1;
        if(g > 1) g = 1;
        if(b > 1) b = 1;
        
        if( _touch )    CGContextSetRGBStrokeColor(context, 1, 1, 1, 1);
        else            CGContextSetRGBStrokeColor(context, r, g, b, 1);
        path = [UIBezierPath bezierPath];
        path.lineWidth = 5;
        
        if( _step == 3 || _step == 5 || _step == 7  ){
            rg = M_PI*2*(cnt-1)/3 + (M_PI*2*rate/3) - M_PI_2;
        }else if( _step >= 8 ){
            rg = M_PI*2 - M_PI_2;
        }else{
            rg = M_PI*2*(cnt-1)/3 - M_PI_2;
        }
        [path addArcWithCenter:center radius:(self.frame.size.width/2 - 6) startAngle:-M_PI_2 endAngle:rg clockwise:YES];
        [path stroke];
        
        CGPoint old = posStar;
        rg += M_PI_2;
        posStar.x = cos(rg)*old.x - sin(rg)*old.y;
        posStar.y = sin(rg)*old.x + cos(rg)*old.y;
        
        //パーティクル登録
        //if( counter%10 == 0 )
        if( _step == 3 || _step == 5 || _step == 7  )
        {
            CGFloat r = (_colorR + 1)*0.5;
            CGFloat g = (_colorG + 1)*0.5;
            CGFloat b = (_colorB + 1)*0.5;
            
            if(r > 1) r = 1;
            if(g > 1) g = 1;
            if(b > 1) b = 1;
            
            for(int i=0; i<5; i++)
            {
                CGFloat size = rand()%20+7;
                CGPoint pos  = CGPointMake(rand()%20-10, rand()%20-10);
                ParticleView *iv = [[ParticleView alloc] initWithImage:_imParticle];
                iv.frame = CGRectMake(self.frame.size.width/2 + posStar.x + pos.x - size/2, self.frame.size.height/2 + posStar.y + pos.y - size/2, size, size);
                iv.contentMode    = UIViewContentModeScaleAspectFit;
                iv.tintColor      = [UIColor colorWithRed:r green:g blue:b alpha:1];
                iv.tag            = 10;
                [self addSubview:iv];
            }
        }
    }/**/
    
    //パーティクル消去
    for (UIImageView *iv in self.subviews) {
        if(iv.tag == 10){
            if( iv.alpha > 0 ){
                iv.alpha -= (rand()%20 + 1)/350.0f;
                iv.transform = CGAffineTransformMakeScale(iv.alpha, iv.alpha);
                iv.transform = CGAffineTransformTranslate(iv.transform, rand()%8, rand()%8);
            }else{
                [iv removeFromSuperview];
            }
        }
    }

    
    _ivStar.frame = CGRectMake(self.frame.size.width/2 + posStar.x - 15, self.frame.size.height/2 + posStar.y - 15, 30, 30);
    _ivStar.hidden = NO;
    
    //縁
    if( _touch )    CGContextSetRGBStrokeColor(context, 1, 1, 1, 1);
    else            CGContextSetRGBStrokeColor(context, _colorR, _colorG, _colorB, 1);
    path = [UIBezierPath bezierPath];
    path.lineWidth = 1;
    [path addArcWithCenter:center radius:(self.frame.size.width/2 - 4) startAngle:0 endAngle:M_PI*2 clockwise:YES];
    [path stroke];
    
    path = [UIBezierPath bezierPath];
    path.lineWidth = 1;
    [path addArcWithCenter:center radius:(self.frame.size.width/2 - 8) startAngle:0 endAngle:M_PI*2 clockwise:YES];
    [path stroke];
    
    counter++;
}
//-----------------------------------------------------------------------------------------
#pragma mark Touch
//-----------------------------------------------------------------------------------------
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _touch = YES;
    [_player play];
    self.userInteractionEnabled = NO;
    
    if( _tapFnc )   _tapFnc(self);
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    _touch = NO;
}
- (NSInteger)tapType{
    NSTimeInterval   sub = fabs([[NSDate date] timeIntervalSince1970] - _touchTime - _duration*2);
    
    NSLog(@"%f", sub);
    
    if( sub < MARK_TOUCH_PERFECT ){
        return TapTypePerfect;
    }else if( sub < MARK_TOUCH_GRATE ){
        return TapTypeGrate;
    }else if( sub < MARK_TOUCH_GOOD ){
        return TapTypeGood;
    }else{
        return TapTypeMiss;
    }
}
//-----------------------------------------------------------------------------------------

@end
