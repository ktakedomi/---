//
//  TapTypeView.m
//  pimms
//
//  Created by kunihiro takedomi on 2015/10/11.
//  Copyright (c) 2015年 yuhodo inc,. All rights reserved.
//

#import "TapTypeView.h"
#import "MarkView.h"

@implementation TapTypeView

- (id)initWithFrame:(CGRect)frame type:(NSInteger)type{
    self = [super initWithFrame:frame];
    if (self) {
        _tapType = type;
        self.contentMode = UIViewContentModeScaleAspectFit;
        
        UIImage     *im;
        switch (_tapType) {
            case TapTypeMiss:
                im = [UIImage imageNamed:@"miss.png"];
                break;
            case TapTypeGood:
                im = [UIImage imageNamed:@"good.png"];
                break;
            case TapTypeGrate:
                im = [UIImage imageNamed:@"grate.png"];
                break;
            case TapTypePerfect:
                im = [UIImage imageNamed:@"perfect.png"];
                break;
        }
        _ivMessage = [[UIImageView alloc] initWithImage:im];
        _ivMessage.frame = CGRectMake(0,0, self.frame.size.width, self.frame.size.height);
        [self addSubview:_ivMessage];
    }
    
    return self;
}

- (void)startAnimate{
    _step = 0;
    [self animeLoop];
}

- (void)animeLoop{
    switch (_step) {
        case 0:{
            _ivMessage.alpha = 0;
            //_ivMessage.transform = CGAffineTransformMakeScale(0.5, 0.5);
            [UIView animateWithDuration:0.1
                                  delay:0.0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 _ivMessage.transform = CGAffineTransformMakeScale(1, 1);
                                 //_ivMessage.transform = CGAffineTransformTranslate(_ivMessage.transform, 0, -10);
                                 _ivMessage.alpha = 1;
                             }completion:^(BOOL finished) {
                                 _step++;
                                 [self animeLoop];
                             }];
        }
            break;
        case 1:{
            [UIView animateWithDuration:0.5f
                                  delay:0.3
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 _ivMessage.alpha = 0;
                                 //_ivMessage.transform = CGAffineTransformTranslate(_ivMessage.transform, 0, 10);
                             }completion:^(BOOL finished) {
                                 [self removeFromSuperview];
                             }];
        }
            break;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
